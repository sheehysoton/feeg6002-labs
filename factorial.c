#include <limits.h>
#include <stdio.h>
#include <math.h>

long maxlong(void){
		long d = LONG_MAX;
		return d;
}

double upper_bound(long n){
	double q;
	if(n < 6){
		q = 1;
		while(n>1){ /* Compute factorial of n */
			q = q * n;
			n--;
		}
		if(q < 720){ /* Return q if q is less than 720, otherwise return 720 */
			return q;
		} else {
			return 720;
		}
	} else {
		q = pow(n/2., n); /* Compute (n/2)^n, always greater than n! */
		return q;
	}
	
}

long factorial(long n){
	if(n < 0){
		return -2;
	} else if(upper_bound(n) > maxlong() ){
		return -1;
	} else {
		long q = 1;
		while(n>1){ /* Compute factorial */
			q = q * n;
			n--;
		}
		return q;
	}
}
	
int main(void) {

    /* The next code block should compile once "upper_bound" is defined. */


    return 0;
}


	
	

